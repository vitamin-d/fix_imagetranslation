<?php
defined('TYPO3_MODE') or die();

(function ($packageKey) {
    \TYPO3\CMS\Core\Utility\GeneralUtility
        ::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \TYPO3\CMS\Extbase\Domain\Model\FileReference::class,
            \VITD\FixImagetranslation\Domain\Model\FileReference::class
        );
})('fix_imagetranslation');
