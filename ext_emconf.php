<?php
$EM_CONF['fix_imagetranslation'] = [
    'title' => 'Allow translating FAL file references',
    'description' => 'Just give editors the power to change files. They cannot translate sys_file_references without '
        . 'always getting the same file. Let\'s just trust them to do no harm. ' . LF
        . 'Furthermore, extbase later invariably uses the default language FAL reference\'s uid to get the file '
        . 'itself, which is wrong again. (This does not apply when not using extbase, obviously.)',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'version' => '87.1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
        ],
    ],
];
