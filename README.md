Fix TYPO3 image translation
===========================

Allows access to `sys_file_reference` file attributes. That way, editors can *change* the file associated with a
file reference. When normally "translating" a `sys_file_reference` it is incredibly hard to convince TYPO3 not to
copy the associated file into the newly created "translated" object. And changing after the fact? That's what this
extension allows you to do.

## Usage

Install and activate. Editors now can *change* the file associated with a `sys_file_reference` and especially so in
translated files

## Features

* zero configuration, just install and done
* minimal system impact

## Bugs

If you find any bugs: Please report them.
