<?php

namespace VITD\FixImagetranslation\Domain\Model;

use TYPO3\CMS\Core\Resource\ResourceFactory;

/**
 * A file reference object (File Abstraction Layer)
 */
class FileReference extends \TYPO3\CMS\Extbase\Domain\Model\FileReference
{

    /**
     * Get the *real* FileReference
     *
     * This variation to the upstream follows language variants
     *
     * @return \TYPO3\CMS\Core\Resource\FileReference
     */
    public function getOriginalResource()
    {
        if ($this->originalResource === null) {
            $uid = (is_int($this->_localizedUid) && $this->_localizedUid > 0)
                ? $this->_localizedUid
                : $this->getUid();
            $this->originalResource = ResourceFactory::getInstance()->getFileReferenceObject($uid);
        }

        return $this->originalResource;
    }
}
