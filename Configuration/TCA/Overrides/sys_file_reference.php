<?php
defined('TYPO3_MODE') or die();


call_user_func(function ($table) {
    $GLOBALS['TCA'][$table]['palettes']['filePalette']['isHiddenPalette'] = false;
},'sys_file_reference');
